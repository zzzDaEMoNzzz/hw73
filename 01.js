const express = require('express');

const app = express();
const port = 8000;

app.get('/', (req, res) => {
  res.send('Main page');
});

app.get('/:route', (req, res) => {
  res.send(req.params.route);
});

app.listen(port, () => {
  console.log('We are live on http://localhost:' + port);
});