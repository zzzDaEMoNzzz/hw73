const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const ENC_PASS = 'password';

const app = express();
const port = 8000;

app.get('/encode/:text', (req, res) => {
  res.send(Vigenere.Cipher(ENC_PASS).crypt(req.params.text));
});

app.get('/decode/:text', (req, res) => {
  res.send(Vigenere.Decipher(ENC_PASS).crypt(req.params.text));
});

app.listen(port, () => {
  console.log('We are live on http://localhost:' + port);
});