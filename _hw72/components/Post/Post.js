import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

const Post = ({title, thumbnail}) => {
  return (
    <View style={styles.Post}>
      <Image source={{uri: thumbnail}} style={styles.Post_image}/>
      <Text style={styles.Post_text}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Post: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    borderColor: '#eee',
    borderWidth: 1,
    backgroundColor: '#fafafa',
    marginTop: 3,
    marginBottom: 3,
    minHeight: 70,
  },
  Post_image: {
    width: 60,
    height: 60,
    position: 'absolute',
    left: 5,
  },
  Post_text: {
    padding: 5,
    paddingLeft: 65,
  },
});

export default Post;
