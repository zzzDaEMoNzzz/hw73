import React, {Component} from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';
import {connect} from "react-redux";
import {getPosts} from "../../store/actions";
import Post from "../../components/Post/Post";

class Posts extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  onEndReached = ({layoutMeasurement, contentOffset, contentSize}) => {
    if (layoutMeasurement.height + contentOffset.y >= contentSize.height) {
      this.props.getPosts(this.props.lastPostID);
    }
  };

  render() {
    return (
      <View style={styles.Posts}>
        <FlatList
          data={this.props.posts}
          keyExtractor={item => item.id}
          renderItem={({item}) => <Post title={item.title} thumbnail={item.thumbnail}/>}
          onScroll={({nativeEvent}) => this.onEndReached(nativeEvent)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Posts: {
    paddingLeft: 5,
    paddingRight: 5,
  }
});

const mapStateToProps = state => ({
  posts: state.posts,
  lastPostID: state.lastPostID,
});

const mapDispatchToProps = dispatch => ({
  getPosts: id => dispatch(getPosts(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);