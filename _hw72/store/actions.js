import axios from 'axios';

export const GET_POSTS = 'GET_POSTS';

export const getPosts = id => {
  return dispatch => {
    let url = 'https://www.reddit.com/r/pics.json';
    if (id) url += `?count=25&after=${id}`;

    axios.get(url).then(response => {
      const result = response.data;
      const children = result.data.children;

      const posts = children.reduce((array, post, index) => {
        if (index > 0) {
          array.push({id: post.data.id, thumbnail: post.data.thumbnail, title: post.data.title})
        }
        return array;
      }, []);

      dispatch({type: GET_POSTS, posts, lastPostID: result.data.after});
    });
  };
};