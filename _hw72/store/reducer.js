import {GET_POSTS} from "./actions";

const initialState = {
  posts: [],
  lastPostID: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        posts: [
          ...state.posts,
          ...action.posts,
        ],
        lastPostID: action.lastPostID,
      };
    default:
      return state;
  }
};

export default reducer;